const forumForm = document.querySelector("#form")

forumForm.addEventListener("submit", (e)=>{
    e.preventDefault();
    const name = document.querySelector("#name")
    const post = document.querySelector("#postarea")

// validation
if (name.value.trim() === "" || post.value.trim() === "") {
    window.alert("missing name or post");
    return;
} 
 // create elements

    const pTag = document.createElement("p");
    const hTag = document.createElement("h1")
    const buttonTag = document.createElement("button")
    const divTag = document.createElement("div")
    const id = Math.floor(Math.random()*100)

        divTag.setAttribute("id", id)
        divTag.setAttribute("class", "postitem")
        hTag.setAttribute("id", "postheading")
    
        const textName = document.createTextNode(`${name.value}`)
        hTag.appendChild(textName);
        divTag.appendChild(hTag)
    
        const textPost = document.createTextNode(`${post.value}`)
        pTag.appendChild(textPost);
        divTag.appendChild(pTag)
      
        const deleteButton = document.createTextNode("Delete")
        buttonTag.appendChild(deleteButton)
        divTag.appendChild(buttonTag)
        buttonTag.addEventListener("click", (e)=>{
             e.preventDefault();
            const elem = document.getElementById(id);
            elem.remove()
        })

    document.getElementById("postlist").appendChild(divTag)


})

function clearfunc(){
    const name = document.querySelector("#name")
    const post = document.querySelector("#postarea")

    name.value === ""
    post.value === ""
}
